package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	//商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String FILE_NOT_DIGIT = "合計金額が10桁を超えました";
	private static final String FILE_NOT_BRANCH_CODE ="の支店コードが不正です";
	private static final String FILE_NOT_FORMAT ="のフォーマットが不正です";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {

		//コマンドライン引数が渡されているか確認する方法
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,"^[0-9]{3}$","支店")) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,"^[0-9a-zA-Z]{8}$","商品")) {
			return;
		}
		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)

		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length ; i++) {
			String name = files[i].getName();
			//ファイルなのか確認する方法
			if(files[i].isFile() && name.matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
				return;
			}
		}

		//売上ファイルが連番か確認する方法
		Collections.sort(rcdFiles);
		for(int i = 0; i<rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter =  Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		for(int i = 0; i <rcdFiles.size(); i++) {
			BufferedReader br = null;
			String line;
			try{
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				List<String> fileContents = new ArrayList<>();
				while(( line = br.readLine()) != null) {
					fileContents.add(line);
				}

				//売上ファイルのフォーマットを確認する方法
				if(fileContents.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NOT_FORMAT);
					return;
				}

				//支店定義ファイルに特定のkeyが存在するか
				if(!branchSales.containsKey(fileContents.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NOT_BRANCH_CODE);
					return;
				}

				//商品定義ファイルに特定のkeyが存在するか
				if(!commoditySales.containsKey(fileContents.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_NOT_BRANCH_CODE);
					return;
				}

				//売上金額が数字なのか確認する方法
				if(!fileContents.get(2).matches("^[0-9]*$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSale = Long.parseLong(fileContents.get(2));


				Long saleAmount = branchSales.get(fileContents.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(fileContents.get(1)) + fileSale;

				//売上金額の合計が10桁を超えたか確認
				if(saleAmount >= 10000000000L  || commodityAmount >= 10000000000L) {
					System.out.println(FILE_NOT_DIGIT);
					return;
				}


				branchSales.put(fileContents.get(0), saleAmount);
				commoditySales.put(fileContents.get(1), commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}

		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0],FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> fileNames, Map<String, Long> fileSales, String fileFormat, String exist) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//エラー処理（ファイルの存在確認）
			if(!file.exists()) {
				System.out.println(exist + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//支店ファイルのフォーマットを確認
				if((items.length != 2) || (!items[0].matches(fileFormat))) {
					System.out.println(exist + FILE_INVALID_FORMAT);
					return false;
				}

				fileNames.put(items[0], items[1]);
				fileSales.put(items[0], 0L);

				//System.out.println(line);
			}

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				};
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> fileNames, Map<String, Long> fileSales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : fileSales.keySet()) {
				bw.write(key + "," + fileNames.get(key) + "," + fileSales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		}  finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				};
			}
		}
		return true;
	}

}
